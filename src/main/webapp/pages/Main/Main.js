Application.$controller("MainPageController", ['$scope', 'Widgets', 'Variables', 'i18nService',
    function($scope, Widgets, Variables, i18nService) {
        "use strict";

        /* perform any action with the variables inside this block(on-page-load) */
        $scope.onPageVariablesReady = function() {
            /*
             * variables can be accessed through 'Variables' service here
             * e.g. Variables.staticVariable1.getData()
             */
        };


        $scope.select1Change = function($event, $isolateScope) {
            i18nService.setSelectedLocale($isolateScope.datavalue);
        };

    }
]);